package com.example.data;


public class PrintPhonesData implements Comparable<PrintPhonesData> {
    private String full_name;
    private String homePhone;
    private String mobilePhone;
    private String primaryPhone;
    private String workPhone;
    private String email;
    private String birthDay;
    private String birthMonth;
    private String birthYear;


    public PrintPhonesData() {
    }

    public PrintPhonesData(String full_name, String homePhone, String mobilePhone, String primaryPhone, String workPhone, String email,
                           String birthDay, String birthMonth, String birthYear) {
        this.full_name = full_name;
        this.homePhone = homePhone;
        this.mobilePhone = mobilePhone;
        this.primaryPhone = primaryPhone;
        this.workPhone = workPhone;
        this.email = email;
        this.birthDay = birthDay;
        this.birthMonth = birthMonth;
        this.birthYear = birthYear;
    }

    public String getFirst_name() {
        return full_name;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public String getPrimaryPhone() {
        return primaryPhone;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public String getEmail() {
        return email;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public String getBirthMonth() {
        return birthMonth;
    }

    public String getBirthYear() {
        return birthYear;
    }


    public PrintPhonesData setHomePhone(String homePhone) {
        this.homePhone = homePhone;
        return this;
    }

    public PrintPhonesData setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
        return this;
    }

    public PrintPhonesData setPrimaryPhone(String primaryPhone) {
        this.primaryPhone = primaryPhone;
        return this;
    }

    public PrintPhonesData setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public void setBirthMonth(String birthMonth) {
        this.birthMonth = birthMonth;
    }

    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
    }


    @Override
    public String toString() {
        return "PrintPhonesData{" +
                "first_name='" + full_name + '\'' +
                ", homePhone='" + homePhone + '\'' +
                ", mobilePhone='" + mobilePhone + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PrintPhonesData that = (PrintPhonesData) o;

        if (full_name != null ? !full_name.equals(that.full_name) : that.full_name != null) return false;
        return primaryPhone != null ? primaryPhone.equals(that.primaryPhone) : that.primaryPhone == null;

    }

    @Override
    public int hashCode() {
        int result = full_name != null ? full_name.hashCode() : 0;
        result = 31 * result + (primaryPhone != null ? primaryPhone.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(PrintPhonesData other) {
        return this.full_name.toLowerCase().compareTo(other.full_name.toLowerCase());

    }

    public PrintPhonesData withFull_name(String full_name) {
        this.full_name = full_name;
        return this;
    }
}
