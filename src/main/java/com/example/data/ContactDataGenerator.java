package com.example.data;


import static com.example.data.RandomizedData.*;

public class ContactDataGenerator {
    public static ContactsData getStandardContact(){
        return new ContactsData.Builder()
                .setFirst_name(generateRandomStringFirst_name())
                .setLast_name(generateRandomStringLast_name())
                .setAddress1(generateRandomStringAddress1())
                .setHomePhone(generateRandomStringMobile())
                .setMobilePhone(generateRandomStringMobile())
                .setEmail(generateRandomEmail())
                .build();
    }

}
