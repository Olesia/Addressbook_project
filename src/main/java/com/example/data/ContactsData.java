package com.example.data;


public class ContactsData implements Comparable<ContactsData> {
    private String first_name;
    private String last_name;
    private String address1;
    private String mobilePhone;
    private String homePhone;
    private String email;
    private String birthDay;
    private String birthMonth;
    private String birthYear;


    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getBirthMonth() {
        return birthMonth;
    }

    public void setBirthMonth(String birthMonth) {
        this.birthMonth = birthMonth;
    }

    public String getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
    }


    @Override
    public String toString() {
        return "ContactsData{" +
                "first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContactsData that = (ContactsData) o;

        if (first_name != null ? !first_name.equals(that.first_name) : that.first_name != null) return false;
        if (last_name != null ? !last_name.equals(that.last_name) : that.last_name != null) return false;
        return email != null ? email.equals(that.email) : that.email == null;

    }

    @Override
    public int hashCode() {
        int result = first_name != null ? first_name.hashCode() : 0;
        result = 31 * result + (last_name != null ? last_name.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(ContactsData other) {
        return this.last_name.toLowerCase().compareTo(other.last_name.toLowerCase());
    }


    ////////////////////////////////////////////////////////------------------------------------------------------------------
    public static class Builder {

        ContactsData contactData = new ContactsData();

        public Builder() {
        }

        public Builder(ContactsData oldContactData) {
            if (oldContactData == null) return;
            contactData.first_name = oldContactData.first_name;
            contactData.last_name = oldContactData.last_name;
            contactData.address1 = oldContactData.address1;
            contactData.email = oldContactData.email;
            contactData.mobilePhone = oldContactData.mobilePhone;
            contactData.homePhone = oldContactData.homePhone;
            contactData.birthDay = oldContactData.birthDay;
            contactData.birthMonth = oldContactData.birthMonth;
            contactData.birthYear = oldContactData.birthYear;
        }



        public Builder setFirst_name(String first_name) {
            contactData.first_name = first_name;
            return this;
        }

        public Builder setLast_name(String last_name) {
            contactData.last_name = last_name;
            return this;
        }

        public Builder setAddress1(String address1) {
            contactData.address1 = address1;
            return this;
        }


        public Builder  setMobilePhone(String mobilePhone) {
            contactData.mobilePhone = mobilePhone;
            return this;
        }


        public Builder setHomePhone(String homePhone) {
            contactData.homePhone = homePhone;
            return this;
        }

        public Builder setEmail(String email) {
            contactData.email = email;
            return this;
        }

        public Builder setBirthDay(String birthDay) {
            contactData.birthDay = birthDay;
            return this;
        }

        public Builder setBirthMonth(String birthMonth) {
            contactData.birthMonth = birthMonth;
            return this;
        }

        public Builder setBirthYear(String birthYear) {
            contactData.birthYear = birthYear;
            return this;
        }

        public ContactsData build() {
            return contactData;
        }
    }
}

