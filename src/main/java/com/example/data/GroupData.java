package com.example.data;

public class GroupData implements Comparable<GroupData>{
    private String name;
    private String header;
    private String footer;

    public String getName() {
        return name;
    }

    public String getHeader() {
        return header;
    }

    public String getFooter() {
        return footer;
    }

    public GroupData setName(String name) {
        this.name = name;
        return this;

    }

    public GroupData setHeader(String header) {
        this.header = header;
        return this;
    }

    public GroupData setFooter(String footer) {
        this.footer = footer;
        return this;
    }

    @Override
    public String toString() {
        return "GroupData{" +
                "name='" + name + '\'' +
                ", header='" + header + '\'' +
                ", footer='" + footer + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GroupData groupData = (GroupData) o;

        return name.equals(groupData.name);
    }

    @Override
    public int compareTo(GroupData other) {
        return this.name.toLowerCase().compareTo(other.name.toLowerCase());
    }


    //////////////////////////////////////////////////////////////////

    public static class Builder {

        GroupData groupData = new GroupData();

        public Builder() {
        }

        public Builder(GroupData oldGroupData) {
            if (oldGroupData == null) return;
            groupData.name = oldGroupData.name;
            groupData.header = oldGroupData.header;
            groupData.footer = oldGroupData.footer;
        }



       public Builder setName(String name) {
           groupData.name = name;
           return this;
    }

    public  Builder setHeader(String header) {
        groupData.header = header;
        return this;
    }

    public  Builder setFooter(String footer) {
        groupData.footer = footer;
        return this;
    }
    public GroupData build (){
        return groupData; }

    }
}
