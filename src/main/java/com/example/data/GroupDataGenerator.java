package com.example.data;

/**
 * Created by oklymenko on 10/19/2016.
 */
public class GroupDataGenerator {
    //public static GroupData getRandomGroup()
    public static GroupData getStandardGroup (){
         return new GroupData.Builder()
                .setName(RandomizedData.generateRandomString())
                .setHeader(RandomizedData.generateRandomString())
                .setFooter(RandomizedData.generateRandomString())
                .build();
    }
}
