package com.example.data;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;


public class RandomizedData {

    public static String generateRandomString() {
        Random rnd = new Random();
        if (rnd.nextInt(3) == 0) {
            return "";
        } else {
            return "test" + rnd.nextInt();
        }
    }


    public static String generateRandomStringLast_name() {
        Random rnd = new Random();
        return "Last name" + (rnd.nextInt(20));
    }

    public static String generateRandomStringFirst_name() {
        Random rnd = new Random();
        return "First name" + (rnd.nextInt(20));
    }

    public static String generateRandomStringAddress1() {
        Random rnd = new Random();
        if (rnd.nextInt(3) == 0) {
            return "";
        } else {
            return "Kyiv, Pushkina str. " + (rnd.nextInt(10) + 1);
        }
    }

    public static String generateRandomStringMobile() {
        Random rnd = new Random();
        return "+3" + (rnd.nextInt() + 1);
    }

    public static String generateRandomEmail() {
        Random rnd = new Random();
        if (rnd.nextInt(3) == 0) {
            return "";
        } else {
            return "test" + (rnd.nextInt(10) + 1) + "@gmail.com";
        }
    }
}
