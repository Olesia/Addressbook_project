package com.example.blocks;

import com.example.data.ContactsData;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

@Name("Contacts row block")
@FindBy(xpath = "//tr[@name='entry']")
public class ContactRowBlock extends HtmlElement{

    public ContactsData getContactData() {
        String last_name = getLastName();
        String first_name = getFirstName();
        String email = getEmail();
        return new ContactsData.Builder().setLast_name(last_name).setFirst_name(first_name).setEmail(email).build();
    }

    @Name("Last name contact field")
    @FindBy(xpath = ".//td[2]")
    private WebElement lastNameField;

    @Name("First name contact field")
    @FindBy(xpath = ".//td[3]")
    private WebElement firstNameField;

    @Name("Email contact field")
    @FindBy(xpath = ".//td[4]")
    private WebElement emailField;

    @Name("Contact editButton")
    @FindBy(xpath = ".//img[@title='Edit']")
    private Button editContactButton;


    private String getLastName() {
        return lastNameField.getText();
    }

    private String getFirstName() {
        return firstNameField.getText();
    }

    private String getEmail() { return emailField.getText(); }

    public void clickEdit() {
        editContactButton.click();
    }

}
