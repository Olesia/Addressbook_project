package com.example.blocks;


import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.CheckBox;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

@Name("Group checkboxes block")
@FindBy(name = "selected[]")
public class GroupCheckboxBlock extends HtmlElement {

        public String getName() {
        return getAttribute("title").substring("Select (".length(), getAttribute("title").length() - ")".length());
    }

    public void select() {
        new CheckBox(getWrappedElement()).select();
    }
}
