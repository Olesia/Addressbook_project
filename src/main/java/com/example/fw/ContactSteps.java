package com.example.fw;


import com.example.data.ContactsData;
import com.example.data.PrintPhonesData;
import com.example.pages.*;
import com.example.utils.SortedListOf;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


public class ContactSteps extends HelperBase {
    private static Logger LOG = LoggerFactory.getLogger(ContactSteps.class);
    public static boolean CREATION = true;
    public static boolean MODIFICATION = false;

    public ContactSteps(ApplicationManager manager) {
        super(manager);
    }

    private SortedListOf<ContactsData> cachedContacts;

    public SortedListOf<ContactsData> getContacts() {
        if (cachedContacts == null) {
            rebuildCache();
        }
        return cachedContacts;
    }


    private void rebuildCache() {
        cachedContacts =  new HomePage(manager).get()
                .getContactDataList();
    }

    @Step("Create Contact \"{0}\"")
    public ContactSteps createContact(ContactsData contacts) {
        LOG.info("Create new: " + contacts);
        new HomePage(manager).get()                 ///
                .openCreateContactPage()
                .fillContactsForm(contacts, CREATION)
                .submitContactsForm()
                .returnToHomePage();
        rebuildCache();
        return this;
    }

    @Step("Modify Contact \"{0}\"")
    public ContactSteps modifyContact(ContactsData targetContact, ContactsData resultContact) {
        LOG.info("Modify: "+ targetContact);
        new HomePage(manager).get()
                .initDeleteEditContact(targetContact)
                .fillContactsForm(resultContact, MODIFICATION)
                .submitContactsModification()
                .returnToHomePage();
        rebuildCache();
        return this;
    }

    @Step("Delete Contact \"{0}\"")
    public ContactSteps removeContact(ContactsData targetContact) {
        LOG.info("Delete: "+ targetContact);
        new HomePage(manager).get()
                .initDeleteEditContact(targetContact)
                .deleteContact()
                .returnToHomePage();
        rebuildCache();
        return this;
    }

    //---------------------------------------------------------------------------


    ////////get contacts from main page
    public List<PrintPhonesData> getPrintPhonesFromMainPage() {
        List<PrintPhonesData> printPhones = new ArrayList<>();
        List<WebElement> trList = driver.findElements(By.xpath("//tr[@name='entry']"));
        for (WebElement tr : trList) {
            PrintPhonesData printPhonesFromMainPage = getPrintPhonesFromMainPage(tr);
            printPhones.add(printPhonesFromMainPage);
        }
        return printPhones;
    }

    private PrintPhonesData getPrintPhonesFromMainPage(WebElement tr) {
        WebElement checkbox = tr.findElement(By.name("selected[]"));

        String title = checkbox.getAttribute("title");
        String text = title.substring("Select (".length(), title.length() - ")".length());
        String full_name = text.trim();

        String phone = tr.findElement(By.xpath(".//td[5]")).getText();
        //сделать прверку - если нет домашнего телефона - проверять по мобильному
        // if (x=!0){
        // primaryPhone= homePhone}
        //else{
        // primaryPhone=  mobilePhone}
        return new PrintPhonesData().withFull_name(full_name).setHomePhone(phone);

    }


    /////////////// PrintPhonesData List
    public List<PrintPhonesData> getPrintPhones() {
        List<PrintPhonesData> printPhone = new ArrayList<>();
        List<WebElement> trPhonesList = driver.findElements(By.xpath("//td[@valign='top']"));
        for (WebElement tr : trPhonesList) {
            PrintPhonesData printPhoneData = getPrintPhones(tr);
            printPhone.add(printPhoneData);
        }
        return printPhone;

    }

    private PrintPhonesData getPrintPhones(WebElement tr) {

        String full_name = tr.findElement(By.xpath(".//b")).getText();

        PrintPhonesData printPhoneData = new PrintPhonesData()
                .withFull_name(full_name);

        String title = tr.getText();
        String[] list = title.split("\\n+");

        for (String line : list) {
            if (line.matches("H:\\s\\S+")) {
                printPhoneData.setHomePhone(line.replaceFirst("H: ", "").trim());
            } else if (line.matches("M:\\s\\S+")) {
                printPhoneData.setMobilePhone(line.replaceFirst("M: ", "").trim());
            } else if (line.matches("W:\\s\\S+")) {
                printPhoneData.setWorkPhone(line.replaceFirst("W: ", "").trim());
            }
        }
        if (list[0].matches("A-Z:\\s\\S+")) {
            printPhoneData.setPrimaryPhone(list[0].replaceFirst("A-Z//: ", "").trim());
        } else {
            printPhoneData.setPrimaryPhone("");
        }
        return printPhoneData;
    }

    public ContactSteps gotoPrintPhonePage() {
        manager.driver.findElement(By.linkText("print phones")).click();
        return this;
    }


    //////////////// PrintAll Methods
/*
    public List<ContactsData> getPrintAllContacts() {
        List<ContactsData> printAllContacts = new ArrayList<>();
        List<WebElement> trPrintAllList = driver.findElements(By.xpath("//td[@valign='top']"));
        for (WebElement tr : trPrintAllList) {
            ContactsData contactsData = getPrintAllContacts(tr);
            printAllContacts.add(contactsData);
        }
        return printAllContacts;

    }

    private PrintPhonesData getPrintAllContacts(WebElement tr) {

        String full_name = tr.findElement(By.xpath(".//b")).getText();


        ContactsData contactsData = new ContactsData() //
                .getFirst_name(full_name);

        String title = tr.getText();
        String[] list = title.split("\\n+");

        for (String line : list) {
            if (line.matches("@\\s\\S+")) {
                contactsData.setEmail().trim());//
            } else if (line.matches("M:\\s\\S+")) {
                contactsData.setMobilePhone(line.replaceFirst("M: ", "").trim());
            } else if (line.matches("W:\\s\\S+")) {
                contactsData.setWorkPhone(line.replaceFirst("W: ", "").trim());
            }
        }
        if (list[0].matches("A-Z:\\s\\S+")) {
            contactsData.setPrimaryPhone(list[0].replaceFirst("A-Z//: ", "").trim());
        } else {
            contactsData.setPrimaryPhone("");
        }

        return contactsData;
    }

*/


    public boolean isContactCreated(ContactsData targetContact) {
        try {
            new HomePage(manager).get().getContactRow(targetContact);
            return true;

        } catch (NoSuchElementException ignore) {
            return false;
        }
    }



}









