package com.example.fw;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;


public class CustomTestListener extends TestListenerAdapter {

    private Logger LOG = LoggerFactory.getLogger(CustomTestListener.class);

    @Override
    public void onTestStart(ITestResult result) {
        LOG.info("- - - - - - - - - - - - - - - - - - - - - - - - - - - - ");
        LOG.info("Test class started: " + result.getTestClass().getName());
        LOG.info("Test started: " + result.getName());

    }

    @Override
    public void onTestSuccess(ITestResult result) {
        LOG.info("Test SUCCESS: " + result.getName());
        LOG.info("- - - - - - - - - - - - - - - - - - - - - - - - - - - - ");
    }

    @Override
    public void onTestFailure(ITestResult result) {
        //makeScreenshot(ScreenshotMaker.getScreenShotFileName(result.getName(), true));
        //LOG.info(WebDriverFactory.getDriver().getCurrentUrl());
        LOG.info("Test FAILED: " + result.getName());
        LOG.info("- - - - - - - - - - - - - - - - - - - - - - - - - - - - ");
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        //makeScreenshot(ScreenshotMaker.getScreenShotFileName(result.getName(), true));

        LOG.info("Test Skipped: " + result.getName());
        //LOG.info(WebDriverFactory.getDriver().getCurrentUrl());
        LOG.info("- - - - - - - - - - - - - - - - - - - - - - - - - - - - ");
    }

//    @Attachment(value = "{0}", type = "image/png")
//    private byte[] makeScreenshot(String screenshotName) {
//        return ScreenshotMaker.captureAshot();
//    }

}
