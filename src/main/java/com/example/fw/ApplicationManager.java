package com.example.fw;


import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class ApplicationManager {

    public static WebDriver driver;
    public String baseUrl;
    private GroupSteps groupSteps;
    private ContactSteps contactSteps;

    ///////////////
    private static String key = null;
    private static int count = 0;
    private static String defaultHub = null;

    ////////////////////////
    public ApplicationManager(String browser) {
        if (browser.equalsIgnoreCase("firefox")) {
            driver = new FirefoxDriver();
        } else if (browser.equalsIgnoreCase("chrome")) {
            File driverChromePath = new File(System.getProperty("user.dir") + "\\src\\main\\resources\\chromedriver\\chromedriver.exe");
            System.setProperty("webdriver.chrome.driver", driverChromePath.getAbsolutePath());

            driver = new ChromeDriver();
        }

        baseUrl = "http://localhost/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get(baseUrl + "/addressbookv4.1.4/");
    }

    public void stop() {
        driver.quit();
    }


    public GroupSteps getGroupSteps() {
        if (groupSteps == null) {
            groupSteps = new GroupSteps(this);
        }
        return groupSteps;
    }

    public ContactSteps getContactHelper() {
        if (contactSteps == null) {
            contactSteps = new ContactSteps(this);
        }
        return contactSteps;
    }

    ////////////////

    public static void setDefaultHub(String newDefaultHub) {
        defaultHub = newDefaultHub;
    }

    private static WebDriver getDriver(String hub, Capabilities capabilities) {
        count++;
        // 1. WebDriver instance is not created yet
        if (driver == null) {
            return newWebDriver(hub, capabilities);
        }
        // 2. Different flavour of WebDriver is required
        String newKey = capabilities.toString() + ":" + hub;
        if (!newKey.equals(key)) {
            dismissDriver();
            key = newKey;
            return newWebDriver(hub, capabilities);
        }
        // 3. Browser is dead
        try {
            driver.getCurrentUrl();
        } catch (Throwable t) {
            t.printStackTrace();
            return newWebDriver(hub, capabilities);
        }
        return driver;
    }


    public static WebDriver getDriver(Capabilities capabilities) {
        return getDriver(defaultHub, capabilities);
    }

    public static void dismissDriver() {
        if (driver != null) {
            try {
                driver.quit();
                driver = null;
                key = null;
            } catch (WebDriverException ex) {
                // it can already be dead or unreachable
            }
        }
    }

    // Factory internals

    private static WebDriver newWebDriver(String hub, Capabilities capabilities) {
        driver = (hub == null)
                ? createLocalDriver(capabilities)
                : createRemoteDriver(hub, capabilities);
        key = capabilities.toString() + ":" + hub;
        count = 0;
        return driver;
    }

    private static WebDriver createRemoteDriver(String hub, Capabilities capabilities) {
        try {
            return new RemoteWebDriver(new URL(hub), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new Error("Could not connect to WebDriver hub", e);
        }
    }

    private static WebDriver createLocalDriver(Capabilities capabilities) {
        String browserType = capabilities.getBrowserName();
        if (browserType.equals("firefox"))
            return new FirefoxDriver(capabilities);
        if (browserType.equals("chrome"))
            return new ChromeDriver(capabilities);
        throw new Error("Unrecognized browser type: " + browserType);
    }

    static {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                dismissDriver();
            }
        });


    }
}

