package com.example.fw;

import com.example.data.GroupData;

import com.example.pages.GroupPage;
import com.example.utils.SortedListOf;
import org.openqa.selenium.NoSuchElementException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.yandex.qatools.allure.annotations.Step;


public class GroupSteps extends HelperBase {

    private static Logger LOG = LoggerFactory.getLogger(GroupSteps.class);
    public GroupSteps(ApplicationManager manager) {
        super(manager);
    }

    private SortedListOf<GroupData> cachedGroups;

    public SortedListOf<GroupData> getGroups() {
        if (cachedGroups == null) {
            rebuildCache();
        }
        return cachedGroups;
    }


    private void rebuildCache() {
        cachedGroups = new GroupPage(manager).get()
                .getGroupDataList();
    }

    @Step("Create Group \"{0}\"")
    public GroupSteps createGroup(GroupData group) {
       LOG.info("Create new: " + group);
        new GroupPage(manager).get()
                .initGroupCreation()
                .fillGroupForm(group)
                .submitGroupCreation()
                .returnToGroupsPage();
        rebuildCache();
        return this;
    }

    @Step("Modify Group \"{0}\"")
    public GroupSteps modifyGroup(GroupData targetGroup, GroupData resultGroup) {
        LOG.info("Modify: " + targetGroup);
        new GroupPage(manager).get()
                .initGroupModification(targetGroup)
                .fillGroupForm(resultGroup)
                .submitGroupModification()
                .returnToGroupsPage();
        rebuildCache();
        return this;
    }

    @Step("Delete Group \"{0}\"")
    public GroupSteps deleteGroup(GroupData targetGroup) {
        LOG.info("Delete: " + targetGroup);
        new GroupPage(manager).get()
                .submitGroupDeletion(targetGroup)
                .returnToGroupsPage();
        rebuildCache();
        return this;
    }


    public boolean isGroupCreated(GroupData targetGroup) {
        try{new GroupPage(manager).get().getGroupDataCheckbox(targetGroup.getName());
            return true;
        }
        catch(NoSuchElementException ignore){
            return false;
        }
    }
}
