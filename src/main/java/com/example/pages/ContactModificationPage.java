package com.example.pages;

import com.example.data.ContactsData;
import com.example.fw.ApplicationManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextInput;

import static com.google.common.truth.Truth.assert_;


public class ContactModificationPage extends PageBase<ContactModificationPage> {

    private static final String URL = "edit.php?id"; ///
    private static Logger LOG = LoggerFactory.getLogger(HomePage.class);

    @FindBy(name = "firstname")
    private TextInput firstname;

    @FindBy(name = "lastname")
    private TextInput lastname;

    @FindBy(name = "address")
    private TextInput address1;

    @FindBy(name = "home")
    private TextInput homePhone;

    @FindBy(name = "mobile")
    private TextInput mobilePhone;

    @FindBy(name = "email")
    private TextInput email;

    @FindBy(name = "bday")
    private TextInput birthDay;

    @FindBy(name = "bmonth")
    private TextInput birthMonth;

    @FindBy(name = "byear")
    private TextInput birthYear;

    @FindBy(xpath = "(//input[@name='update'])[1]")
    private Button submitContactsModification;

    @FindBy(xpath = "(//input[@name='update'])[2]")
    private Button deleteContact;

    public ContactModificationPage(ApplicationManager app) {
        super(app, URL);
    }

    @Step
    public ContactModificationPage fillContactsForm(ContactsData contacts, boolean formType) {
        LOG.info("Modify contact with "+ contacts);
        type(firstname, contacts.getFirst_name());
        type(lastname, contacts.getLast_name());
        type(address1, contacts.getAddress1());
        type(homePhone, contacts.getHomePhone());
        type(mobilePhone, contacts.getMobilePhone());
        type(email, contacts.getEmail());

        selectByText(birthDay, contacts.getBirthDay());
        selectByText(birthMonth, contacts.getBirthMonth());

        type(birthYear, contacts.getBirthYear());
        return this;
    }

    @Step
    public ConfirmationContactPage submitContactsModification() {
        LOG.info("Submit contact modification");
        submitContactsModification.click();
        return new ConfirmationContactPage(app);
    }

    @Step
    public ConfirmationContactPage deleteContact() {
        LOG.info("Click Delete contact");
        deleteContact.click();
        return new ConfirmationContactPage(app);
    }

    @Override
    public boolean isOnPage() {
        return super.isOnPage()&& deleteContact.isDisplayed();
    }

    @Override
    protected void load() {

    }

    @Override
    protected void isLoaded() {
    assert_().withFailureMessage("Update button is not loaded")
            .that(submitContactsModification.isDisplayed()).isTrue();
    }
}
