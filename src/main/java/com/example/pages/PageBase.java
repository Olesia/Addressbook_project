package com.example.pages;


import com.example.fw.ApplicationManager;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;
import ru.yandex.qatools.htmlelements.loader.HtmlElementLoader;
import ru.yandex.qatools.htmlelements.loader.decorator.proxyhandlers.TypifiedElementListNamedProxyHandler;

import static com.google.common.truth.Truth.assert_;


public abstract class PageBase<T extends PageBase<T>> {

    protected ApplicationManager app;
    private String url;

    public PageBase(ApplicationManager app, String url) {
        HtmlElementLoader.populatePageObject(this, app.driver);

        //PageFactory.initElements(app.driver, this);
        this.app = app;
        this.url = url;
    }

    public T get() {
        if (isOnPage()) {
            isLoaded();
            return (T) this;
        } else {
            load();
            return checkNavigation();
        }
    }

    protected abstract void load();

    protected abstract void isLoaded();


    public boolean isOnPage() {
        return app.driver.getCurrentUrl().contains(url);
    }

    public T checkNavigation() {
        assert_().withFailureMessage("User is on page: " + app.driver.getCurrentUrl())
                .that(isOnPage()).named("User should be on " + this.url).isTrue();
        isLoaded();
        return (T) this;
    }


    protected void type (TypifiedElement field, String text) {
        if (text != null) {
            field.clear();
            field.sendKeys(text);
        }
    }

    protected void selectByText(WebElement element, String text) {
        if (text != null) {
            new Select(element).selectByVisibleText(text);
        }
    }

    protected void click(WebElement element) {
        element.click();
    }




}
