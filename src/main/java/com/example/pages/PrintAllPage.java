package com.example.pages;


import com.example.data.ContactsData;
import com.example.data.PrintPhonesData;
import com.example.fw.ApplicationManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class PrintAllPage extends PageBase {
    private static final String URL = "view.php?all&print";

    public PrintAllPage(ApplicationManager app) {
        super(app, URL);
    }

   /* public List<ContactsData> getPrintAllContacts() {
        List<ContactsData> printAllContacts = new ArrayList<>();
        List<WebElement> trPrintAllList = driver.findElements(By.xpath("//td[@valign='top']"));
        for (WebElement tr : trPrintAllList) {
            ContactsData contactsData = getPrintAllContacts(tr);
            printAllContacts.add(contactsData);
        }
        return printAllContacts;

    }

    private PrintPhonesData getPrintAllContacts(WebElement tr) {

        String full_name = tr.findElement(By.xpath(".//b")).getText();


        ContactsData contactsData = new ContactsData() //
                .getFirst_name(full_name);

        String title = tr.getText();
        String[] list = title.split("\\n+");

        for (String line : list) {
            if (line.matches("@\\s\\S+")) {
                contactsData.setEmail().trim());//
            } else if (line.matches("M:\\s\\S+")) {
                contactsData.setMobilePhone(line.replaceFirst("M: ", "").trim());
            } else if (line.matches("W:\\s\\S+")) {
                contactsData.setWorkPhone(line.replaceFirst("W: ", "").trim());
            }
        }
        if (list[0].matches("A-Z:\\s\\S+")) {
            contactsData.setPrimaryPhone(list[0].replaceFirst("A-Z//: ", "").trim());
        } else {
            contactsData.setPrimaryPhone("");
        }

        return contactsData;
    }*/


    @Override
    protected void load() {

    }

    @Override
    protected void isLoaded() {
    }


}
