package com.example.pages;

import com.example.data.GroupData;
import com.example.fw.ApplicationManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextInput;

import static com.google.common.truth.Truth.assert_;


public class GroupCreationPage extends PageBase<GroupCreationPage> {
    private static Logger LOG = LoggerFactory.getLogger(GroupCreationPage.class);
    private static final String URL = "group.php";
    @Name("Group name field")
    @FindBy(name = "group_name")
    private TextInput group_name;

    @Name("Group header field")
    @FindBy(name = "group_header")
    private TextInput  group_header;

    @Name("Group footer field")
    @FindBy(name = "group_footer")
    private TextInput  group_footer;

    @Name("Group submit button")
    @FindBy(name = "submit")
    private Button buttonSubmit;

    public GroupCreationPage(ApplicationManager app) {
        super(app, URL);
    }

    @Step
    public GroupCreationPage fillGroupForm(GroupData group) {
        LOG.info("Fill group form");
        type(group_name, group.getName());
        type(group_header, group.getHeader());
        type(group_footer, group.getFooter());
        return this;
    }

    @Step
    public ConfirmationGroupsPage submitGroupCreation() {
        LOG.info("Submit group form");
        buttonSubmit.click();
        return new ConfirmationGroupsPage(app);
    }


    @Override
    public boolean isOnPage() {
        return super.isOnPage()&& buttonSubmit.isDisplayed();
    }

    @Override
    protected void load() {
       new GroupPage(app).get().initGroupCreation();
    }

    @Override
    protected void isLoaded() {
        assert_().withFailureMessage("submit button is not loaded").that(buttonSubmit.isDisplayed()).isTrue();

    }
}
