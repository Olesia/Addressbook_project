package com.example.pages;

import com.example.blocks.ContactRowBlock;
import com.example.data.ContactsData;
import com.example.fw.ApplicationManager;
import com.example.utils.SortedListOf;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.yandex.qatools.htmlelements.element.Link;
import ru.yandex.qatools.htmlelements.element.TextInput;

import java.util.List;
import java.util.NoSuchElementException;



import static com.google.common.truth.Truth.assert_;


public class HomePage extends PageBase<HomePage> {
    private static final String URL = "Home Page";
    private static Logger LOG = LoggerFactory.getLogger(HomePage.class);

    @FindBy(linkText = "add new")
    private TextInput newContact;

    @FindBy(linkText = "groups")
    private TextInput group;

    @FindBy(linkText = "print phones")
    private TextInput printPhones;

    @FindBy(xpath = "//img[@alt='Edit']")
    private TextInput editContact;

    public List<ContactRowBlock> contactRowList;

    @FindBy(xpath = ".//*[@id='maintable']/tbody/tr[1]")
    private TextInput table;

    @FindBy(linkText = "print all")
    private Link printAll;


    public HomePage(ApplicationManager app) {
        super(app, URL);
    }

    public boolean isOnPage() {
        return app.driver.findElements(By.id("maintable")).size() > 0;
    }

    @Override
    protected void load() {
        app.driver.findElement(By.linkText("home")).click();

    }

    @Override
    protected void isLoaded() {
        assert_().withFailureMessage("Table is not loaded")
                .that(table.isDisplayed()).isTrue();
    }

    public PrintPhonesPage openPrintPhonesPage() {
        printPhones.click();
        return new PrintPhonesPage(app);
    }
///////
    public PrintAllPage openPrintAllPage(){
        printAll.click();
        return  new PrintAllPage(app);
    }

//////
    public ContactCreationPage openCreateContactPage() {
        LOG.info("Open CreateContactPage");
        newContact.click();
        return new ContactCreationPage(app);
    }


//    public ContactModificationPage initDeleteEditContact(int index) {
//        getContactByIndex(index);                           //
//        editContact.click();
//        return new ContactModificationPage(app);
//    }
//
//    public HomePage getContactByIndex(int index) {          //
//        contactRowList.get(index).click();
//        return this;

    //   }
////////

    public ContactModificationPage initDeleteEditContact(ContactsData targetContact) {
        LOG.info("Init edit contact "+ targetContact);
        getContactRow(targetContact).clickEdit();
        return new ContactModificationPage(app);
    }


    public ContactRowBlock getContactRow(ContactsData targetContact) {      //
        for (ContactRowBlock row : contactRowList) {
            ContactsData contact = row.getContactData();
            if (targetContact.equals(contact)) {
                return row;
            }
        }
        throw new NoSuchElementException("There is no such contact: " + targetContact.toString());
    }



    public SortedListOf<ContactsData> getContactDataList() {
        SortedListOf<ContactsData> cachedContacts = new SortedListOf<ContactsData>();
        for (ContactRowBlock row : contactRowList) {
            cachedContacts.add(row.getContactData());
        }
        return cachedContacts;
    }



    //////////

//   public ContactsData getContactsData(WebElement tr) {
//        String last_name = tr.findElement(By.xpath("./td[2]")).getText();
//        String first_name = tr.findElement(By.xpath("./td[3]")).getText();
//        return new ContactsData.Builder()
//                .setLast_name(last_name)
//                .setFirst_name(first_name)
//                .build();
//    }
//
//    public SortedListOf<ContactsData> getContactDataList() {
//        SortedListOf<ContactsData> contactsList = new SortedListOf<ContactsData>();
//        for (WebElement tr : contactRowList) {
//            ContactsData contactData = getContactsData(tr);
//            contactsList.add(contactData);
//        }
//        return contactsList;
//    }

    public GroupPage openGroupPage() {
        group.click();
        //app.getCurrentUrl().contains("/group.php");
        return new GroupPage(app);
    }


}
