package com.example.pages;


import com.example.fw.ApplicationManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfirmationGroupsPage extends PageBase <ConfirmationGroupsPage> {
    private static final String URL = "group.php";
    private static Logger LOG = LoggerFactory.getLogger( ConfirmationGroupsPage.class);

    @FindBy(linkText="group page")
    private WebElement groupPagLink;


    public ConfirmationGroupsPage(ApplicationManager app) {
        super(app, URL);
    }

    public HomePage returnToGroupsPage() {
        LOG.info("Return to Groups page");
        groupPagLink.click();
        return new HomePage(app);
    }


    @Override
    protected void load() {

    }

    @Override
    protected void isLoaded() {

    }
}
