package com.example.pages;

import com.example.blocks.GroupCheckboxBlock;
import com.example.data.GroupData;
import com.example.fw.ApplicationManager;
import com.example.utils.SortedListOf;
import org.openqa.selenium.By;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;

import java.util.List;
import java.util.NoSuchElementException;

import static com.google.common.truth.Truth.assert_;


public class GroupPage extends PageBase <GroupPage> {
    private static Logger LOG = LoggerFactory.getLogger(GroupPage.class);
    private static final String URL = "group.php";

    @Name("Group button New")
    @FindBy(name = "new")
    private Button buttonNewGroup;

    private List<GroupCheckboxBlock> checkboxesGroup;

    @Name("Group button Edit")
    @FindBy(name = "edit")
    private Button buttonEditGroup;

    @Name("Group button Delete")
    @FindBy(name = "delete")
    private Button buttonDeleteGroup;

    public GroupCreationPage openCreateGroupPage() {
        buttonNewGroup.click();
        return new GroupCreationPage(app);
    }

//    public GroupModificationPage openEditGroupPage() {   ///
//        buttonEditGroup.click();
//        return new GroupModificationPage(app);
//    }

    public GroupPage(ApplicationManager app) {
        super(app, URL);
    }


    @Override     ///
    public boolean isOnPage() {
       return (app.driver.getCurrentUrl().contains("/group.php")
                && app.driver.findElements(By.name("new")).size() > 0);
           }


    @Override
    protected void load() {
    new HomePage(app).get().openGroupPage();
    }

    @Override
    protected void isLoaded() {
        assert_().withFailureMessage("New group button is not loaded").that(buttonNewGroup.isDisplayed()).isTrue();
    }

    public SortedListOf<GroupData> getGroupDataList() {
        SortedListOf<GroupData> groupDataList = new SortedListOf<>();
        for (GroupCheckboxBlock checkbox : checkboxesGroup) {
            String name = checkbox.getName();
            groupDataList.add(new GroupData.Builder().setName(name).build());
        }
        return groupDataList;
    }



    @Step
    public GroupCreationPage initGroupCreation() {
        LOG.info("Click New Group button");
        buttonNewGroup.click();
        return new GroupCreationPage(app);
    }

    @Step
    public GroupModificationPage initGroupModification(GroupData targetGroup) {
        LOG.info("Check group for modification");
        getGroupDataCheckbox(targetGroup);
        buttonEditGroup.click();
        return new GroupModificationPage(app);
    }

    private void getGroupDataCheckbox(GroupData targetGroup) {
        getGroupDataCheckbox(targetGroup.getName()).select();
    }

    @Step
    public ConfirmationGroupsPage submitGroupDeletion(GroupData targetGroup) {
        LOG.info("Submit group deletion");
        getGroupDataCheckbox(targetGroup);
        buttonDeleteGroup.click();
        return new ConfirmationGroupsPage(app);
    }

    public GroupCheckboxBlock getGroupDataCheckbox(String groupName) {
        for (GroupCheckboxBlock checkbox : checkboxesGroup) {
            String name = checkbox.getName();
            if (name.contains(groupName)) {
                return checkbox;
            }
        }
        throw new org.openqa.selenium.NoSuchElementException("There is no such group: " + groupName);
    }

//    public GroupPage selectGroupByIndex(int index) {
//        checkboxesGroup.get(index).click();
//        return this;
//    }
}
