package com.example.pages;

import com.example.data.ContactsData;
import com.example.fw.ApplicationManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Select;
import ru.yandex.qatools.htmlelements.element.TextInput;

import java.util.List;

import static com.example.fw.ContactSteps.CREATION;
import static com.google.common.truth.Truth.assert_;


public class ContactCreationPage extends PageBase<ContactCreationPage> {
    private static final String URL = "edit.php";
    private static Logger LOG = LoggerFactory.getLogger(ContactCreationPage.class);

    @Name("Contact firstname input field")
    @FindBy(name = "firstname")
    private TextInput firstname;

    @Name("Contact lastname input field")
    @FindBy(name = "lastname")
    private TextInput lastname;

    @Name("Contact address input field")
    @FindBy(name = "address")
    private TextInput address1;

    @Name("Contact home input field")
    @FindBy(name = "home")
    private TextInput homePhone;

    @Name("Contact mobile input field")
    @FindBy(name = "mobile")
    private TextInput mobilePhone;

    @Name("Contact email input field")
    @FindBy(name = "email")
    private TextInput email;

    @Name("Contact bday input field")
    @FindBy(name = "bday")
    private TextInput birthDay;

    @Name("Contact bmonth input field")
    @FindBy(name = "bmonth")
    private TextInput birthMonth;

    @Name("Contact byear input field")
    @FindBy(name = "byear")
    private TextInput birthYear;


    @FindBy(name = "new_group")
    private Select groupField; //

    @Name("Contact submit button")
    @FindBy(name = "submit")
    private Button submitContactsForm;


    public ContactCreationPage(ApplicationManager app) {
        super(app, URL);
    }

    @Step
    public ContactCreationPage fillContactsForm(ContactsData contacts, boolean formType) {
        LOG.info("Fill contacts form: " + contacts);
        fillFirstName(contacts.getFirst_name());
        fillLastName(contacts.getLast_name());
        fillAdress1(contacts.getAddress1());
        fillHomePhone(contacts.getHomePhone());
        fillMobilePhone(contacts.getMobilePhone());
        fillEmail(contacts.getEmail());
                if (formType == CREATION) {
           // selectByText(By.name("new_group"), "group 1");
        } else {
            if (!groupField.getFirstSelectedOption().getText().equals("[none]")) {
                        throw new Error("Group selector exists in contact modification form");
                    }
        }
        selectByText(birthDay, contacts.getBirthDay());
        selectByText(birthMonth, contacts.getBirthMonth());

        type(birthYear, contacts.getBirthYear());
        return this;
    }


    @Step
    private void fillEmail(String emailField) {
        type(email, emailField);
    }

    @Step
    private void fillMobilePhone(String mobilePhoneField) {
        type(mobilePhone, mobilePhoneField);
    }


    @Step
    private void fillHomePhone(String homePhoneField) {
        type(homePhone, homePhoneField);
    }

    @Step
    private void fillAdress1(String addressField) {
        type(address1, addressField);
    }

    @Step
    private void fillLastName(String last_name) {
        LOG.info("Fill contact last name");
        type(lastname, last_name);
    }

    @Step
    private void fillFirstName(String first_name) {
        LOG.info("Fill contact first name");
        type(firstname, first_name);
    }

    @Step
    public ConfirmationContactPage submitContactsForm() {
        LOG.info("Submit contact form");
        submitContactsForm.click();
        return new ConfirmationContactPage(app);
    }


    @Override
    protected void load() {
        new HomePage(app).get().openCreateContactPage();
    }

    @Override
    protected void isLoaded() {
        assert_().withFailureMessage("First name field is not loaded")
                .that(firstname.isDisplayed()).isTrue();
    }
}
