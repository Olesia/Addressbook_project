package com.example.pages;

import com.example.fw.ApplicationManager;
import org.openqa.jetty.html.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.LoadableComponent;

import static com.google.common.truth.Truth.assert_;


public class ConfirmationContactPage extends PageBase<ConfirmationContactPage> {
    private static final String URL = "edit.php";


    @FindBy(linkText = "home page")
    private WebElement homePageLink;

    public HomePage returnToHomePage() {
        homePageLink.click();
        return new HomePage(app);
    }

    public ConfirmationContactPage(ApplicationManager app) {
        super(app, URL);
    }

    @Override     ///
    public boolean isOnPage() {
        return app.driver.findElements(By.linkText("home page")).size() > 0;
    }

    @Override
    protected void load() {

    }

    @Override
    protected void isLoaded() {
        assert_().withFailureMessage("Home page link is not loaded")
                .that(homePageLink.isDisplayed()).isTrue();


    }
}
