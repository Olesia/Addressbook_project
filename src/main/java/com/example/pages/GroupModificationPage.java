package com.example.pages;

import com.example.data.GroupData;
import com.example.fw.ApplicationManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextInput;


public class GroupModificationPage extends PageBase<GroupModificationPage> {

    private static Logger LOG = LoggerFactory.getLogger(GroupModificationPage.class);
    private static final String URL = "group.php";

    @FindBy(name = "update")
    private Button buttonUpdateGroup;

    @FindBy(name = "group_name")
    private TextInput group_name;

    @FindBy(name = "group_header")
    private TextInput group_header;

    @FindBy(name = "group_footer")
    private TextInput group_footer;

    @FindBy(name = "submit")
    private Button buttonSubmit;

    public GroupModificationPage(ApplicationManager app) {
        super(app, URL);
    }

    @Step
    public GroupModificationPage fillGroupForm(GroupData group) {
        LOG.info("Fill group form");
        type(group_name, group.getName());
        type(group_header, group.getHeader());
        type(group_footer, group.getFooter());
        return this;
    }

    @Step
    public ConfirmationGroupsPage submitGroupCreation() {
        LOG.info("Submit group creation");
        buttonSubmit.click();
        return new ConfirmationGroupsPage(app);
    }

    @Step
    public ConfirmationGroupsPage submitGroupModification() {
       LOG.info("Submit group modification");
        buttonUpdateGroup.click();
        return new ConfirmationGroupsPage(app);
    }

    @Override       ///
    public boolean isOnPage() {
        return super.isOnPage();

    }


    @Override
    protected void load() {
            }

    @Override
    protected void isLoaded() {

    }
}
