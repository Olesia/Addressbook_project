package com.example.tests.group;

import com.example.data.GroupData;
import com.example.tests.TestBase;
import com.example.utils.SortedListOf;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;


public class GroupRemovalTests extends TestBase {

    @DataProvider
    public Object [][] groupRemoval(){
        return new Object [][]{{groupData}};
    }

    @Features({"Removal","group"})
    @Stories("Group Removal")
    @Test(dataProvider = "groupRemoval", groups = {"removal"})
    public void deleteSomeGroup(GroupData targetGroup) {

    // save old state
    SortedListOf<GroupData> oldList = app.getGroupSteps().getGroups();

    //actions
    if(!app.getGroupSteps().isGroupCreated(targetGroup)){
        app.getGroupSteps().createGroup(targetGroup);
    }
        app.getGroupSteps().deleteGroup(targetGroup);

    //save new state
    SortedListOf<GroupData> newList = app.getGroupSteps().getGroups();

    //compare states
    assertThat(newList, equalTo(oldList.without(targetGroup)));

    }

}
