package com.example.tests.group;

import com.example.data.GroupData;
import com.example.data.GroupDataGenerator;
import com.example.tests.TestBase;
import com.example.utils.SortedListOf;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;


public class GroupModificationTest extends TestBase {
    @DataProvider
    public Object [][] groupModification(){
        GroupData resultGroup = GroupDataGenerator.getStandardGroup();
        return new Object[][]{{groupData, resultGroup}};
    }

    @Features({"Modification","group"})
    @Stories("Group modification")
    @Test (dataProvider = "groupModification", groups = {"modification"})
    public void modifySomeGroup(GroupData targetGroup, GroupData resultGroup) {

        // save old state
        SortedListOf<GroupData> oldList = app.getGroupSteps().getGroups();
//        Random rnd = new Random();
//        int index = rnd.nextInt(oldList.size()-1);

        //actions

        if(!app.getGroupSteps().isGroupCreated(targetGroup)){
                app.getGroupSteps().createGroup(targetGroup);
        }
        app.getGroupSteps().modifyGroup(targetGroup, resultGroup);


        //save new state
        SortedListOf<GroupData> newList = app.getGroupSteps().getGroups();

        //compare states
        assertThat(newList, equalTo(oldList.without(targetGroup).withAdded(resultGroup)));
    }

}
