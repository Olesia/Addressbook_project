
package com.example.tests.group;

import com.example.data.GroupData;
import com.example.tests.TestBase;
import com.example.utils.SortedListOf;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;


public class GroupCreationTests extends TestBase {
    @DataProvider
    public Object[][] standardGroup() {
        return new Object[][]{{groupData}};
    }

    @Features({"Creation","group"})
    @Stories("Group creation")
    @Test(dataProvider = "standardGroup", groups = {"creation" })
    public void testGroupCreationWithValidData(GroupData group) throws Exception {

        // save old state
        SortedListOf<GroupData> oldList = app.getGroupSteps().getGroups();

        //actions
        app.getGroupSteps().createGroup(group);


        //save new state
        SortedListOf<GroupData> newList = app.getGroupSteps().getGroups();

        //compare states
        assertThat(newList, equalTo(oldList.withAdded(group)));
    }
}



