package com.example.tests;


import com.example.data.PrintPhonesData;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;

import static org.testng.Assert.assertEquals;

public class PrintPhonesDataTest extends TestBase {


    @Test
    public void testPrintPhonesDataTest() throws Exception {

        // save old state
       List<PrintPhonesData> oldList = app.getContactHelper().getPrintPhonesFromMainPage();

       app.getContactHelper().gotoPrintPhonePage(); //зашли на PrintPhonePage

        //save new state
        List<PrintPhonesData> newList = app.getContactHelper().getPrintPhones();


        //compare states

        Collections.sort(oldList);
        Collections.sort(newList);
        assertEquals(newList, oldList);
        }

}

