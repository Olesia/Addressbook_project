
package com.example.tests.contact;

import com.example.data.ContactsData;
import com.example.tests.TestBase;
import com.example.utils.SortedListOf;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.example.data.RandomizedData;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;



public class ContactCreationTests extends TestBase {

    @DataProvider
    public Object[][] standardContact() {
        return new Object[][]{{contactsData}};
    }

    @Features({"Creation", "contacts"})
    @Stories("Contact creation")
    @Test(dataProvider = "standardContact", groups = {"creation"})
    public void testContactsCreationWithValidData(ContactsData contacts) throws Exception {

        // save old state
        SortedListOf<ContactsData> oldList = app.getContactHelper().getContacts();

        //actions
        app.getContactHelper().createContact(contacts);

        //save new state
        SortedListOf<ContactsData> newList = app.getContactHelper().getContacts();

        //compare states
        assertThat(newList, equalTo(oldList.withAdded(contacts)));

    }


    /*@Test(enabled = false)
    public void contactsDisplayedOnPrintAllPage(ContactsData targetContact) {
        app.getContactHelper().createContact(targetContact);

        SortedListOf<ContactsData> oldList = app.getContactHelper().getContacts();

        if (!app.getContactHelper().isContactCreated(targetContact)) {
            app.getContactHelper().createContact(targetContact);
        }
        app.getContactHelper().isContactDisplayedOnPrintAllPage(targetContact);//


        SortedListOf<ContactsData> newList = app.getContactHelper().getContacts();

        //compare states
        assertThat(newList, equalTo(oldList.withAdded(targetContact)));


    }*/

}