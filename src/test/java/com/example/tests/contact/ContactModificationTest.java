package com.example.tests.contact;


import com.example.data.ContactDataGenerator;
import com.example.data.ContactsData;
import com.example.pages.HomePage;
import com.example.tests.TestBase;
import com.example.utils.SortedListOf;
import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;


import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class ContactModificationTest extends TestBase {

    @DataProvider
    public Object[][] modifyStandardContact() {
        ContactsData resultContact = ContactDataGenerator.getStandardContact();
        return new Object[][]{{contactsData, resultContact}};
    }

    @Features({"Modification", "contacts"})
    @Stories("Contact modification")
    @Test(dataProvider = "modifyStandardContact", groups = {"modification"})
    public void modifySomeContacts(ContactsData targetContact, ContactsData resultContact) {


        // save old state
        SortedListOf<ContactsData> oldList = app.getContactHelper().getContacts();
//       Random rnd = new Random();
//       int index = rnd.nextInt(oldList.size()-1);

        //actions
        if (!app.getContactHelper().isContactCreated(targetContact)) {
            app.getContactHelper().createContact(targetContact);
        }
        app.getContactHelper().modifyContact(targetContact, resultContact);


        //save new state
        SortedListOf<ContactsData> newList = app.getContactHelper().getContacts();

        //compare states

        assertThat(newList, equalTo(oldList.without(targetContact).withAdded(resultContact)));
        contactsData = new ContactsData.Builder(resultContact).build();
    }

}
