package com.example.tests.contact;


import com.example.data.ContactsData;
import com.example.tests.TestBase;
import com.example.utils.SortedListOf;
import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.Iterator;
import java.util.Random;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class ContactRemovalTest extends TestBase {

    @DataProvider
    public Object[][] removeStandardContact() {
        return new Object[][]{{contactsData}};
    }

    @Features({"Remove", "contacts"})
    @Stories("Contact removal")
    @Test(dataProvider = "removeStandardContact", groups = {"removal"})
    public void deleteSomeContacts(ContactsData targetContactGroup) {

        // save old state
        SortedListOf<ContactsData> oldList = app.getContactHelper().getContacts();

        //actions
        if (!app.getContactHelper().isContactCreated(targetContactGroup)) {
            app.getContactHelper().createContact(targetContactGroup);
        }
        app.getContactHelper().removeContact(targetContactGroup);

        //save new state
        SortedListOf<ContactsData> newList = app.getContactHelper().getContacts();


        //compare states
        assertThat(newList, equalTo(oldList.without(targetContactGroup)));

    }

    @Test(enabled = false)
    public void deleteAllContacts() {
        for ( ContactsData contact : app.getContactHelper().getContacts()) {
            app.getContactHelper().removeContact(contact);
        }
    }
}



