package com.example.tests;

import com.example.data.*;
import com.example.fw.ApplicationManager;
import com.example.fw.CustomTestListener;
import org.testng.annotations.*;



@Listeners(CustomTestListener.class)
public class TestBase {

    protected static ApplicationManager app;
    public static ContactsData contactsData;
    public static GroupData groupData;

    @BeforeSuite(alwaysRun = true)
    public void createStandardContactandGroup(){
        contactsData = ContactDataGenerator.getStandardContact();
        groupData = GroupDataGenerator.getStandardGroup();
    }

   // List<Object[]> groupLIst = new ArrayList<>();

    @Parameters("browser")
    @BeforeTest(alwaysRun = true)
    public void setUp(@Optional("") String browser) throws Exception {
        if (browser == null || browser.isEmpty()) {
            browser = System.getenv("browser");
            if (browser == null) {
                browser = System.getProperty("browser");
            }
        }
        app = new ApplicationManager(browser);
    }

    @AfterTest(alwaysRun = true)
    public void tearDown() throws Exception {
        app.stop();

    }


//    @DataProvider
//    public Iterator<Object[]> randomValidGroupsGenerator() {
//        groupLIst = new ArrayList<>();
//        for (int i = 0; i < 5; i++) {
//            GroupData groupData = GroupDataGenerator.getRandomGroup();
//            groupLIst.add(new Object[]{groupData});
//        }
//        return groupLIst.iterator();
//    }
//
//
//    @DataProvider
//    public Iterator<Object[]> randomValidContactGenerator() {
//        List<Object[]> list = new ArrayList<>();
//        for (int i = 0; i < 4; i++) {
//            ContactsData contacts = new ContactsData.Builder()
//                    .setFirst_name(RandomizedData.generateRandomStringFirst_name())
//                    .setLast_name(RandomizedData.generateRandomStringLast_name())
//                    .setAddress1(RandomizedData.generateRandomStringAddress1())
//                    .setEmail(RandomizedData.generateRandomEmail())
//                    .setMobilePhone(RandomizedData.generateRandomStringMobile())
//                    .build();
//
//            list.add(new Object[]{contacts});
//        }
//        return list.iterator();
//    }

//    @DataProvider
//    public Object[][] getGroupDataForEdit() {
//        return new Object[][]{{groupLIst.get(0), GroupDataGenerator.getRandomGroup()}};
//    }


}






